function main(){
    var request = jsonUtils.toObject(json);
    // var result = {};

    var definition = workflow.getDefinitionByName('activiti$corfoWorkflow');
    var defParams = {};
    for(var propName in request){
        defParams[propName.replace('prop_', '').replace('_', ':')] = request[propName];
    }
    
    var path = definition.startWorkflow(workflow.createPackage(), defParams);
    var task = path.getTasks()[0];

    task.endTask('Next');
    task = path.getTasks()[0];
    
    task.endTask('Next');
    task = path.getTasks()[0];

    model.result = jsonUtils.toJSONString({ sucess: true, id: task.id, instanceId: path.getInstance().id });
}

main();