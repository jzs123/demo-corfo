function main(){
    var request = jsonUtils.toObject(json);

    var instanceId, task, result = [];
    for(var i in request.instanceIds){

        instanceId = request.advance[i].instanceIds;
        output = request.advance[i].output;
        task = workflow.getInstance(instanceId).getPaths()[0].getTasks()[0];   
        task.endTask(request.advance[i].output); 
        result.push({ taskId: task.id, output: output })
    }

    model.result = jsonUtils.toJSONString(result);
}

main();