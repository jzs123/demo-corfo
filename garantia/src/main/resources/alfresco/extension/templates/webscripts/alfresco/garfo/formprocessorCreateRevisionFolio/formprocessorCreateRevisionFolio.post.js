var map = function(arr, fun){
	 for(var i in arr)
        if (fun(arr[i], i)) return true;
};

var fnFieldValue = function(p_field)
{
   return p_field.value.length() > 0 && p_field.value != "null" ? p_field.value : null;
};
	
function main(){
//aqui falta cerrar todos los id que vienen en emision de folio
		
	var 

	siteId = args.siteId,
	montoFolio = args.montoFolio,	
	site = siteService.getSite(siteId),
	doclib = site.getContainer("documentLibrary"),
	date = new Date(),
	year = date.getFullYear(),
	month = date.getMonth() + 1,
	filename = null,
	content = null,
	worflowsIds = null;
		
	

	for each (field in formdata.fields)
    {
       switch (String(field.name).toLowerCase())
       {
       case "filename":
           filename = fnFieldValue(field);
           logger.error('name se ve' + filename);
           break;
           
          case "worflowsids":
        	  worflowsIds = fnFieldValue(field);
        	  logger.error('se ve aqui' + worflowsIds);
             break;
          
          case "filedata":
             if (field.isFile)
             {
            	
                filename = filename ? filename : field.filename;
                filename += '';
                content = field.content;
                mimetype = field.mimetype;
                logger.error('filedata ve aqui' + filename);
             }
             break;       
             
       }
    }
	
	var nombreFolio = "";
	var ext = filename.split('.');
	logger.error('extension' + ext[1]);
	
	var foliosFolders = doclib.childByNamePath('Folios');
	if(!foliosFolders){
		foliosFolders = doclib.createNode('Folios', "corf:typFolios");		
	}
	
	var carpetaYear = foliosFolders.childByNamePath(year.toString());
	if(!carpetaYear){
		carpetaYear = foliosFolders.createFolder(year.toString());
	}
	
	var carpetaMonth = carpetaYear.childByNamePath(month.toString());
	if(!carpetaMonth){
		carpetaMonth = carpetaYear.createFolder(month.toString());
		carpetaMonth.addAspect('cm:countable',{'cm:counter': 0});
	}
	logger.error('--------------------------------');
	logger.error(carpetaMonth.properties['cm:counter']);
	var folioName = 'FOL-00' + carpetaMonth.properties['cm:counter'].toString();
	var carpetaFolio = carpetaMonth.childByNamePath(folioName);
	if(!carpetaFolio){
		carpetaFolio = carpetaMonth.createNode(folioName, 'corf:typExpedienteFolio');
		carpetaMonth.properties['cm:counter'] = carpetaMonth.properties['cm:counter'] + 1;
		carpetaMonth.save();
	}	
	
    var 
    newFile = carpetaFolio.createFile(folioName +'.'+ ext[1], 'corf:boleta');
    newFile.properties.content.write(content, false, true);    
    newFile.save();
    
	
    var definition = workflow.getDefinitionByName('activiti$garfoRevision');
    var defParams = {
    		"corf:folio": folioName + '.' + ext[1],
    		"corf:sap": "",
    		"corf:montoComisionPagado": montoFolio,
    		"corf:worflowsIds": worflowsIds,
    		"corf:documentoDePago": newFile.nodeRef,
    		"corf:siteIfi": siteId
    		
    };
    
    var arrayWorkflowsId = worflowsIds.split(',');
    map(arrayWorkflowsId, function(w){
    	logger.error('imprime el flujo ' + w);
    	var task = workflow.getInstance('activiti$' + w).getPaths()[0].getTasks()[0];
    	var properties = task.properties;
    	properties['corf:estadoTarea'] = 'Folio_emitido';
    	properties['corf:montoEmitidoFolio'] = montoFolio;
    	task.setProperties(properties);
    	task.endTask('Next');
    });
    
  
    var path = definition.startWorkflow(workflow.createPackage(), defParams);
   
    var task = path.getTasks()[0];
  
    task.endTask('Next');
    task = path.getTasks()[0];  
	
   
model.result = jsonUtils.toJSONString({
	folioName: folioName,
	nodeRef: newFile.nodeRef.toString(),
	id: task.id
});

}

main();