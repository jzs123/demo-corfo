
var map = function(arr, fun){
	 for(var i in arr)
        if (fun(arr[i], i)) return true;
};

function executeRuleCupoPorBeneficiario(){
	logger.error('incio executeRuleCupoPorBeneficiario');
	var ventasEmpresaAnual = execution.getVariable('corf_ventasEmpresaAnual');
	if(ventasEmpresaAnual != ""){
		if(ventasEmpresaAnual <= 2400){
			execution.setVariable('corf_cupoPorBeneficiario', 5000);	
		}
		if(ventasEmpresaAnual > 2400 && ventasEmpresaAnual <= 25000 ){
			execution.setVariable('corf_cupoPorBeneficiario', 9000);	
		}
		if(ventasEmpresaAnual > 25000 && ventasEmpresaAnual <= 100000 ){
			execution.setVariable('corf_cupoPorBeneficiario', 18000);	
		}
		logger.error('incio executeRuleCupoPorBeneficiario');
	}
	
}
function executeRuleCobertura(){
	logger.error('Inicio executeRuleCobertura');
	var ventasEmpresaAnual = execution.getVariable('corf_ventasEmpresaAnual');
	var numeroCuotas = execution.getVariable('corf_numeroCuotas');
	if(ventasEmpresaAnual != "" && numeroCuotas !=""){
		if(ventasEmpresaAnual <= 2400 && numeroCuotas <= 36){
			execution.setVariable('corf_porcientoCoberturaMaximo', 60);	

		} else if(ventasEmpresaAnual >= 2400 && ventasEmpresaAnual <= 25000 && numeroCuotas <= 36){
			execution.setVariable('corf_porcientoCoberturaMaximo', 60);	
			
		} else if(ventasEmpresaAnual >= 25000 && ventasEmpresaAnual <= 100000 && numeroCuotas <= 36){
			execution.setVariable('corf_porcientoCoberturaMaximo', 40);	
			
		}else if(ventasEmpresaAnual <= 2400 && numeroCuotas > 36 && numeroCuotas <= 60){
			execution.setVariable('corf_porcientoCoberturaMaximo', 80);	
			
		} else if(ventasEmpresaAnual > 2400 && ventasEmpresaAnual <= 25000 && numeroCuotas > 36 && numeroCuotas <= 60){
			execution.setVariable('corf_porcientoCoberturaMaximo', 70);	
			
		} else if(ventasEmpresaAnual > 25000 && ventasEmpresaAnual <= 100000 && numeroCuotas > 36 && numeroCuotas <= 60){
			execution.setVariable('corf_porcientoCoberturaMaximo', 50);	
				
		} else if(ventasEmpresaAnual <= 2400 &&  numeroCuotas > 60){
			execution.setVariable('corf_porcientoCoberturaMaximo', 80);	
			
		} else if(ventasEmpresaAnual > 2400 && ventasEmpresaAnual <= 25000 && numeroCuotas > 60){
			execution.setVariable('corf_porcientoCoberturaMaximo', 80);	
			
		}else if(ventasEmpresaAnual > 25000 && ventasEmpresaAnual <= 100000 && numeroCuotas > 60){
			execution.setVariable('corf_porcientoCoberturaMaximo', 70);	
		
		} 
		var porcientoCobertura = execution.getVariable('corf_montoDeOperacion')*100/execution.getVariable('corf_montoTotalCapital');
		var porcientoCoberturaMaximo = execution.getVariable('corf_porcientoCoberturaMaximo');
		if(porcientoCoberturaMaximo < porcientoCobertura){
			var textoRechazo = execution.getVariable('corf_rechazoReglas');
			execution.setVariable('corf_rechazoReglas', textoRechazo + 'Tasa de cobertura supera el maximo permitido. ');
		}
	}
	
	logger.error('Fin executeRuleCobertura');
}

function executeRuleCalculaComision(){
	logger.error('incio executeRuleCalculaComision');
	  var montoDeOperacion = execution.getVariable('corf_montoDeOperacion');
	  var tasaInteresAnual = execution.getVariable('corf_tasaInteresAnual');
	  var numeroCuotas = execution.getVariable('corf_numeroCuotas');
	  var tasaComision = execution.getVariable('corf_tasaComision');
	  var saldo_inicial = 0;
	  var interesm = (Math.pow((tasaInteresAnual+1),1/12)-1);
	  var cuotasm=((montoDeOperacion * interesm * Math.pow((interesm + 1),  numeroCuotas))/(Math.pow((interesm + 1), numeroCuotas) - 1))>>0;
	  var comisionm = (Math.pow((tasaComision+1),1/12)-1);
	  var capital = 0;
	  var num_cuotas = 0;
	  var interes = 0;
	  var pago = 0;
	  var amortizacion = 0;
	  var saldo_final = 0;
	  var comision_mensual = 0;
	  var suma_comision  = 0;
	  var informacion = [];
	  for ( num_cuotas ;  num_cuotas < numeroCuotas;  num_cuotas ++) {
	    if ( num_cuotas == 0) {
	       saldo_inicial = montoDeOperacion;
	       interes = Math.round(montoDeOperacion*interesm);
	       pago = cuotasm;
	       amortizacion  = Math.round(cuotasm -  interes);
	       saldo_final =  saldo_inicial - amortizacion;
	       comision_mensual = Math.round(montoDeOperacion * comisionm);
	    }else{
	       saldo_inicial = saldo_final;
	       interes = Math.round( saldo_inicial*interesm);
	       pago = cuotasm;
	       amortizacion  = Math.round(cuotasm - interes);
	      if (( saldo_inicial - amortizacion) >=10) {
	         saldo_final = saldo_inicial - amortizacion;
	      }else{
	         saldo_final = 0;
	      }
	       comision_mensual = Math.round( saldo_inicial * comisionm);
	    }
		
			suma_comision = suma_comision + comision_mensual;
	  }
	  execution.setVariable('corf_comisionCalculada', suma_comision);
	  
	  logger.error('fin executeRuleCalculaComision');
	  	  
	};

function executeValidationCupoIfi(){
	logger.error('Inicio executeValidationCupoIfi');
	var montoDeOperacion = execution.getVariable('corf_montoDeOperacion');
	if(montoDeOperacion != ""){
		var
		site = siteService.getSite('garfo'),
		dataListsContainer = site.getContainer("dataLists"),
		dataLists = dataListsContainer.getChildren(),
		c = 0;
		
		map(dataLists,function(dataList){
			if(dataList.getProperties()['dl:dataListItemType'] == 'corf:typIfi'){
				map(dataList.children,function(children){	
					//if(children.getProperties()['corf:rutIfi'] == execution.getVariable('corf_dListRutIfi')){
						var saldoGastadoIfi = children.getProperties()['corf:saldoGastadoIfi'];
						var montoAprobadoIfi = children.getProperties()['corf:montoAprobadoIfi'];
						if(montoDeOperacion > (montoAprobadoIfi - saldoGastadoIfi)){
							var textoRechazo = execution.getVariable('corf_rechazoReglas');
							execution.setVariable('corf_rechazoReglas', textoRechazo + 'Se supera el cupo máximo por IFI. ');
						}
					//}				
				});
			}
		});
	}
	logger.error('Fin executeValidationCupoIfi');
}

/*function executeValidationCupoCliente(){
	logger.error('Inicio executeValidationCupoCliente');
	var montoDeOperacion = execution.getVariable('corf_montoDeOperacion');
	if(montoDeOperacion != ""){
		var
		site = siteService.getSite('garfo'),
		dataListsContainer = site.getContainer("dataLists"),
		dataLists = dataListsContainer.getChildren(),
		c = 0;
		
		map(dataLists,function(dataList){
			if(dataList.getProperties()['dl:dataListItemType'] == 'corf:typCliente'){
				map(dataList.children,function(children){	
					if(children.getProperties()['corf:rutCliente'] == execution.getVariable('corf_rut')){
						var saldoGastado = children.getProperties()['corf:saldoGastado'];
						var montoAprobado = children.getProperties()['corf:montoAprobado'];
						if(montoDeOperacion > (montoAprobado - saldoGastado)){
							var textoRechazo = execution.getVariable('corf_rechazoReglas');
							execution.setVariable('corf_rechazoReglas', textoRechazo + 'Se supera el cupo máximo otorgado. ');
						}
					}				
				});
			}
		});
	}
	logger.error('Fin executeValidationCupoCliente');
}
*/
function executeValidationFechaOperacion(){
	logger.error('Inicio executeValidationFechaOperacion');
	var fechaAprobacion = execution.getVariable('corf_fechaAprobacion');
	if(fechaAprobacion != ""){
		var now = new Date();
		logger.error('executeValidationFechaOperacion' + fechaAprobacion.toString());
		logger.error('executeValidationFechaOperacion' + now.toString());
		logger.error(fechaAprobacion.getTime() < now.getTime());
		if(fechaAprobacion.getTime() < now.getTime()){
			logger.error('executeValidationFechaOperacion comparacion lista');
			logger.error('executeValidationFechaOperacion' + now.toString());
			var textoRechazo = execution.getVariable('corf_rechazoReglas');
			execution.setVariable('corf_rechazoReglas', textoRechazo + 'Fecha de operación esta fuera de plazo. ');
		}	
	}	
	logger.error('Fin executeValidationFechaOperacion');
}

function executeValidationGracia(){
	logger.error('Inicio executeValidationGracia');
	var mesesPeriodoGracia = execution.getVariable('corf_mesesPeriodoGracia');
	var tipoGracia = execution.getVariable('corf_tipoGracia');
	var numeroCuotas = execution.getVariable('corf_numeroCuotas');
	var textoRechazo = execution.getVariable('corf_rechazoReglas');
	if(numeroCuotas != "" && mesesPeriodoGracia != "" ){
		if(numeroCuotas < mesesPeriodoGracia){
			execution.setVariable('corf_rechazoReglas', textoRechazo + 
					'Meses del período de garantía no puede ser mayor que el número de cuotas. ');			
		}
		if(mesesPeriodoGracia == 0 && tipoGracia!= 0){
			execution.setVariable('corf_rechazoReglas', textoRechazo + 
					'Periodo de Gracia no es consistente con tipo de gracia seleccionado. ');
		}
	}	
	logger.error('Fin executeValidationGracia');
}

function executeValidationFechaPrimerVencimiento(){
	logger.error('Inicio executeValidationFechaPrimerVencimiento');
	var fechaPrimerPago = execution.getVariable('corf_fechaPrimerPago');
	var fechaAprobacion = execution.getVariable('corf_fechaAprobacion');
	var textoRechazo = execution.getVariable('corf_rechazoReglas');
	if(fechaPrimerPago != "" && fechaAprobacion != ""){
		logger.error('executeValidationFechaPrimerVencimiento');
		logger.error(fechaPrimerPago.getTime() <= fechaAprobacion.getTime());
		if(fechaPrimerPago.getTime() <= fechaAprobacion.getTime()){
			execution.setVariable('corf_rechazoReglas', textoRechazo + 
					'Fecha de primer vencimiento no puede ser inferior a fecha de aprobación. ');
		}
	}
	logger.error('Fin executeValidationFechaPrimerVencimiento');
}

function executeValidationMontoGarantizado(){
	logger.error('Inicio executeValidationMontoGarantizado');
	var montoDeOperacion = execution.getVariable('corf_montoDeOperacion');
	var montoTotalCapital = execution.getVariable('corf_montoTotalCapital');
	var textoRechazo = execution.getVariable('corf_rechazoReglas');
	if(montoDeOperacion != "" && montoTotalCapital != ""){
		if(montoDeOperacion >= montoTotalCapital){
			execution.setVariable('corf_rechazoReglas', textoRechazo + 
					'Monto de operación a garantizar no puede ser superior al monto del crédito. ');
		}
	}
	logger.error('Fin executeValidationMontoGarantizado');
}

function executeValidationVentaEmpresaAnual(){
	logger.error('Fin executeValidationVentaEmpresaAnual');
	var ventasEmpresaAnual = execution.getVariable('corf_ventasEmpresaAnual');
	var textoRechazo = execution.getVariable('corf_rechazoReglas');
	if(ventasEmpresaAnual > 100000){
		execution.setVariable('corf_rechazoReglas', textoRechazo + 
		'Las ventas de la empresa no deben exceder las UF 100000.0000 . ');
	}
	logger.error('Fin executeValidationVentaEmpresaAnual');
}





function executeTransition(){
	var textoRechazo = execution.getVariable('corf_rechazoReglas');
	logger.error('executeTransition texto rechazo '+ textoRechazo);
	logger.error( textoRechazo);
	logger.error(textoRechazo);
	if(!textoRechazo){
		execution.setVariable('corf_assignee', 'GROUP_CORFO_APROBACION');	
		execution.setVariable('corf_estadoTarea', 'Pre aprobado');
	}else{
		execution.setVariableLocal('corf_estadoTarea', 'Pre aprobado cancelado');	
		logger.error('estado: '+ execution.getVariable('corf_estadoTarea'));
	
	}
}

function changeValueCupoIfi(monto, change){
	logger.error('Inicio changeValueCupoIfi');
	
	if(monto != ""){
		var
		site = siteService.getSite('garfo'),
		dataListsContainer = site.getContainer("dataLists"),
		dataLists = dataListsContainer.getChildren(),
		c = 0;
		
		map(dataLists,function(dataList){
			if(dataList.getProperties()['dl:dataListItemType'] == 'corf:typIfi'){
				map(dataList.children,function(children){	
					//if(children.getProperties()['corf:rutIfi'] == execution.getVariable('corf_dListRutIfi')){
						var saldoGastadoIfi = children.getProperties()['corf:saldoGastadoIfi'];
						var montoAprobadoIfi = children.getProperties()['corf:montoAprobadoIfi'];
						if(montoAprobadoIfi >= (saldoGastadoIfi + monto)){
							if(change == true){
								logger.error('old valor ifi ' + children.properties['corf:saldoGastadoIfi'].toString());
								children.properties['corf:saldoGastadoIfi'] = saldoGastadoIfi + monto;
								children.save();
								logger.error('Nuevo valor ifi ' + children.properties['corf:saldoGastadoIfi'].toString());
							}														
						}else{
							var textoRechazo = execution.getVariable('corf_rechazoReglas');
							execution.setVariable('corf_rechazoReglas', textoRechazo + 'Se supera el cupo máximo por IFI. ');
						}
					//}				
				});
			}
		});
	}
	logger.error('Fin changeValueCupoIfi');
}

/*function changeValueCupoCliente(monto, change){
	logger.error('Inicio changeValueCupoCliente');
	
	if(monto != ""){
		var
		site = siteService.getSite('garfo'),
		dataListsContainer = site.getContainer("dataLists"),
		dataLists = dataListsContainer.getChildren(),
		c = 0;
		
		map(dataLists,function(dataList){
			if(dataList.getProperties()['dl:dataListItemType'] == 'corf:typCliente'){
				map(dataList.children,function(children){	
					if(children.getProperties()['corf:rutCliente'] == execution.getVariable('corf_rut')){
						var saldoGastado = children.getProperties()['corf:saldoGastado'];
						var montoAprobado = children.getProperties()['corf:montoAprobado'];
						if(montoAprobado >= (saldoGastado + monto)){
							if(change == true){
								logger.error('Old valor' + children.properties['corf:saldoGastado'].toString());
								children.properties['corf:saldoGastado'] = saldoGastado + monto;
								children.save();
								logger.error('Nuevo valor' + children.properties['corf:saldoGastado'].toString());
							}							
						}else{
							var textoRechazo = execution.getVariable('corf_rechazoReglas');
							execution.setVariable('corf_rechazoReglas', textoRechazo + 'Se supera el cupo máximo otorgado. ');
						}
					}				
				});
			}
		});
	}
	logger.error('Fin changeValueCupoCliente');
}*/




