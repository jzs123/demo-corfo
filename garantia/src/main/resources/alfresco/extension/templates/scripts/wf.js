<import resource="classpath:alfresco/extension/templates/scripts/rules.js">; 
//213
/**
 * 
 */
	 var //imports
	 ContextLoader = Packages.org.springframework.web.context.ContextLoader,
	 SecuentialService = Packages.cl.dox.repo.secuentials.SecuentialService,
	 ServiceRegistry = Packages.org.alfresco.service.ServiceRegistry,
	 ActivitiScriptNode = Packages.org.alfresco.repo.workflow.activiti.ActivitiScriptNode;
	 
	 var GRUPO_ERROR = "GRUPO NO ENCONTRADO, REVISE LA CONFIGURACION DE GRUPOS CON SU ADMINISTRADOR.";
	 var EMAILFROM = 'alfresco@corfrum.cl';
 	var srv = function(bname){
 		return ContextLoader.getCurrentWebApplicationContext().getBean(bname, this[bname]);
	};
	

	
	var getEmailGroupMembers = function(g) {
	    var mails = [];
	    var g = people.getGroup(g);
	    people.getMembers(g).map(function(p) {
	    	mails.push(p.properties["cm:email"]);
	    });
	    return mails;
	};
	var sendMailNodo = function(pobj) {
		  var mail = actions.create("mail");
		  var templateModel = [];
		  templateModel['opcionalVar'] = pobj.opcionalVar;
		  mail.parameters.template_model = templateModel;
		  mail.parameters.to = pobj.to;
		  mail.parameters.subject = pobj.subject;
		  mail.parameters.from = EMAILFROM;
		  mail.parameters.template = search.luceneSearch("workspace://SpacesStore", "+@cm\\:name:\"" + pobj.template + "\"")[0];
		  mail.parameters.text = 'no body';
		  //mail.executeAsynchronously(companyhome);
		  mail.executeAsynchronously(companyhome);		  
		  logger.error("finalizando email");
		};
	var notificacionUno = function(_a) {
	    sendMailNodo({
	        template: 'uno.ftl',
	        to: _a,
	        subject: 'Notificación Tesorería',
	        opcionalVar: {
	            estado: "estado tarea"
	        }
	    });

	 };
	 
	 var GROUP_PATRON_PREAPROBADO = 'GROUP_{0}_PREAPROBADO';
	 var GROUP_PATRON_PREAPROBADOLOTE = 'GROUP_{0}_PREAPROBADOLOTE';
	 var GROUP_PATRON_APROBADOCORFO = 'GROUP_{0}_APROBADOCORFO';
	 var GROUP_PATRON_EMISIONFOLIO = 'GROUP_{0}_EMISIONFOLIO';
	 var GROUP_PATRON_REVISIONFOLIO = 'GROUP_{0}_REVISIONFOLIO';
	 
	 
	 function setGroup(pattern, client, target){
	    var grp = pattern.replace(/\{0\}/, client);
	    execution.setVariable(target, grp);
	    logger.error('Corfo::setGroup - ' + target +'={1}'+ target+ grp);
	}
	 
	 
	function takeInicio(){
		logger.error('takeInicio '+ execution.getVariable('initiator'));
	}
	
	function entrando(){
		logger.error('entrando');
		
	}
	
	function saliendo(){
		logger.error('saliendo');
		var ifi = execution.getVariable('corf_siteIfi').toUpperCase();
		setGroup(GROUP_PATRON_PREAPROBADO, ifi, 'corf_estadoPreAprobado');
		setGroup(GROUP_PATRON_PREAPROBADOLOTE, ifi, 'corf_estadoPreAprobadoPorLote');
		setGroup(GROUP_PATRON_APROBADOCORFO, ifi, 'corf_estadoAprobadoCorfo');
		setGroup(GROUP_PATRON_EMISIONFOLIO, ifi, 'corf_estadoEmisionFolio');
		
	}
		
	function takeInicioPreAprobado(){
		logger.error('takeInicioPreAprobado');
		//execution.setVariable('corf_assignee', 'GROUP_CORFO_APROBACION');
		var grupo = execution.getVariable('corf_estadoPreAprobado');
	    if (!grupo) 
	        throw GRUPO_ERROR;
	    logger.error('ASIGNANDO A GRUPO >>>' + grupo);
	    var arr = [];
	    arr["corf_estadoPreAprobado"] = grupo;
	    getEmailGroupMembers(grupo).map(function(s1) {
	        notificacionUno(s1);
	    });
	   	logger.error('finaliza ');
	}
	
	function createPreAprobado(){
		logger.error('Inicio createPreAprobado');
		executeRuleCobertura();
		executeRuleCalculaComision();
		executeRuleCupoPorBeneficiario();
		executeValidationCupoIfi();
		//executeValidationCupoCliente();
		executeValidationFechaOperacion();
		executeValidationGracia();
		executeValidationFechaPrimerVencimiento();
		executeValidationMontoGarantizado();
		executeValidationVentaEmpresaAnual();	
		
		logger.error(' Fin createPreAprobado');
	}
	
	function completePreAprobado(){
		logger.error(' Inicio completePreAprobado');
		task.setVariable('corf_estadoTarea', 'Pre_aprobado_Lote');	
	}	
	
	function takePreAprobadoPorLote(){
		logger.error('takePreAprobadoPorLote');
		//execution.setVariable('corf_assignee', 'GROUP_CORFO_APROBACION');
		var grupo = execution.getVariable('corf_estadoPreAprobadoPorLote');
	    if (!grupo) 
	        throw GRUPO_ERROR;
	    logger.error('ASIGNANDO A GRUPO >>>' + grupo);
	    var arr = [];
	    arr["corf_estadoPreAprobadoPorLote"] = grupo;
	    getEmailGroupMembers(grupo).map(function(s1) {
	        notificacionUno(s1);
	    });
	   	logger.error('finaliza ');
	}	
	
	function createAprobadoPorLote(){
		logger.error('createAprobadoPorLote');		
		var textoRechazo = execution.getVariable('corf_rechazoReglas');
		logger.error("textoRechazo "+ textoRechazo);
		if(!textoRechazo){			
			execution.setVariable('corf_estadoTarea', 'Aprobado_en_lote');
			task.setVariable('corf_estadoTarea', 'Aprobado_en_lote');						
		}else{
			logger.error('createAprobadoPorLote '+ textoRechazo);
			execution.setVariable('corf_estadoTarea', 'Rechazado_en_lote');
			task.setVariable('corf_estadoTarea', 'Rechazado_en_lote');
			
		}	
		logger.error('estado: '+ execution.getVariable('corf_estadoTarea'));
		logger.error('Fin createAprobadoPorLote');		
	}
	
	function completeAprobadoPorLote(){
		logger.error('completeAprobadoPorLote');
		execution.setVariable('corf_estadoTarea',task.getVariable('corf_estadoTarea') );
//		if(task.getVariable('corfwf_preAprobadoPorLoteDecision') == 'Rechazado_en_lote'){
//			execution.setVariable('corf_estadoTarea', 'Rechazado_en_lote');
//			task.setVariable('corf_estadoTarea', 'Rechazado_en_lote');
//		}else{
//			execution.setVariable('corf_estadoTarea', 'Aprobado_en_lote');
//			task.setVariable('corf_estadoTarea', 'Aprobado_en_lote');
//		}
		logger.error('estado: '+ task.getVariable('corf_estadoTarea'));
	}
	
	function takeFinAprobadoPorLote(){
		logger.error('takeFinAprobadoPorLote');
	}
	
	function takeRechazadoPorLote(){
		logger.error('createAprobadoPorLote');
	}
	
	function takePreAprobacionCorfo(){
		logger.error('takePreAprobacionCorfo');
	//	execution.setVariable('corf_assignee', 'GROUP_CORFO_APROBACION');
		var grupo = execution.getVariable('corf_estadoAprobadoCorfo');
	    if (!grupo) 
	        throw GRUPO_ERROR;
	    logger.error('ASIGNANDO A GRUPO >>>' + grupo);
	    var arr = [];
	    arr["corf_estadoAprobadoCorfo"] = grupo;
	    getEmailGroupMembers(grupo).map(function(s1) {
	        notificacionUno(s1);
	    });
		
	   	logger.error('finaliza ');
	}
	
	function createPreAprobacionCorfo(){
		logger.error('createPreAprobacionCorfo');
		executeValidationCupoIfi();
		//executeValidationCupoCliente();
		var textoRechazo = execution.getVariable('corf_rechazoReglas');
		logger.error(textoRechazo);
		if(!textoRechazo){ 
			execution.setVariable('corf_estadoTarea', 'Emision_Folio');			
			task.setVariable('corf_estadoTarea', 'Emision_Folio');
		}else{
			logger.error('createPreAprobacionCorfo '+ textoRechazo);
			execution.setVariable('corf_estadoTarea', 'Rechazado_por_Corfo');
			task.setVariable('corf_estadoTarea', 'Rechazado_por_Corfo');
		}	
		logger.error('estado: '+ execution.getVariable('corf_estadoTarea'));
	}
	
	function completePreAprobacionCorfo(){
		logger.error('Inicio completePreAprobacionCorfo');
		execution.setVariable('corf_estadoTarea',task.getVariable('corf_estadoTarea') );
//		logger.error(task.getVariable('corfwf_preAprobacionCorfoDecision'));
//		logger.error(execution.getVariable('corfwf_preAprobacionCorfoDecision'));
//		if(task.getVariable('corfwf_preAprobacionCorfoDecision') == 'Emision_Folio'){
//			execution.setVariable('corf_estadoTarea', 'Emision_Folio');
//			task.setVariable('corf_estadoTarea', 'Emision_Folio');
//		}else{
//			execution.setVariable('corf_estadoTarea', 'Rechazado_por_Corfo');
//			task.setVariable('corf_estadoTarea', 'Rechazado_por_Corfo');
//		}
//		logger.error('estado: '+ execution.getVariable('corf_estadoTarea'));
		logger.error('Fin completePreAprobacionCorfo');
	}
	
	function takeFinPreAprobacionCorfo(){
		logger.error('takeFinPreAprobacionCorfo');
		
	}
	
	function takePreAprobacionCorfoRechazado(){
		logger.error('takePreAprobacionCorfoRechazado');
	}
	
	function takeEmisionFolio(){
		logger.error(' takeEmisionFolio');
		//execution.setVariable('corf_assignee', 'GROUP_CORFO_APROBACION');
		var grupo = execution.getVariable('corf_estadoEmisionFolio');
	    if (!grupo) 
	        throw GRUPO_ERROR;

	    logger.error('ASIGNANDO A GRUPO >>>' + grupo);
	    var arr = [];
	    arr["corf_estadoEmisionFolio"] = grupo;
	    getEmailGroupMembers(grupo).map(function(s1) {
	        notificacionUno(s1);
	    });
	    logger.error('finaliza ');
		
	}
	
	function createEmisionFolio(){
		logger.error('createEmisionFolio');
		executeValidationCupoIfi();
		//executeValidationCupoCliente();
		var textoRechazo = execution.getVariable('corf_rechazoReglas');
		logger.error('createEmisionFolio texto rechazo '+ textoRechazo);		
		if(!textoRechazo){
			execution.setVariable('corf_estadoTarea', 'Folio_emitido');
			execution.setVariable('corf_montoEmitidoFolio', task.getVariable('corf_montoEmitidoFolio'));
			task.setVariable('corf_estadoTarea', 'Folio_emitido');
			execution.setVariable('corf:montoEmitidoFolio', task.getVariable('corf:montoEmitidoFolio'));
		}else{
			logger.error('createEmisionFolio '+ textoRechazo);
			execution.setVariable('corf_estadoTarea', 'Folio_no_pagado');
			task.setVariable('corf_estadoTarea', 'Folio_no_pagado');
		}
		logger.error('estado: '+ execution.getVariable('corf_estadoTarea'));
	}
	
	function completeEmisionFolio(){
		logger.error('inicio completeEmisionFolio');
		execution.setVariable('corf_estadoTarea',task.getVariable('corf_estadoTarea') );
//		if(task.getVariable('corfwf_folioEmisionDecision') == 'Folio_emitido'){
//			execution.setVariable('corf_estadoTarea', 'Folio_emitido');
//			task.setVariable('corf_estadoTarea', 'Folio_emitido');
//		}else{
//			execution.setVariable('corf_estadoTarea', 'Folio_no_pagado');
//			task.setVariable('corf_estadoTarea', 'Folio_no_pagado');
//		}
//		logger.error('estado: '+ execution.getVariable('corf_estadoTarea'));
		logger.error('fin completeDisponibleFolio');
	}

	//TAKE---------------------------------------
	
	
	
	function inicioWRevision(){
		logger.error('inicioWRevision');
		
	}
	
	function endWRevision(){
		logger.error('endWRevision');
		var ifi = execution.getVariable('corf_siteIfi').toUpperCase();
		setGroup(GROUP_PATRON_REVISIONFOLIO, ifi, 'corf_estadoRevisionFolio');
	}
	
	function takePreRevisionFolio(){
		logger.error('takePreRevisionFolio');
		//execution.setVariable('corf_assignee', 'GROUP_CORFO_APROBACION');
		var grupo = execution.getVariable('corf_estadoRevisionFolio');
		logger.error('takePreRevisionFolio ' + grupo);
	    if (!grupo) 
	        throw GRUPO_ERROR;

	    logger.error('ASIGNANDO A GRUPO >>>' + grupo);
	    var arr = [];
	    arr["corf_estadoRevisionFolio"] = grupo;
	    getEmailGroupMembers(grupo).map(function(s1) {
	        notificacionUno(s1);
	    });
	    logger.error('finaliza ');
	}
	
	function createRevisionFolio(){
		logger.error('createRevisionFolio');
		// execution.setVariable('corf:documentoDePago', task.getVariable('corf:documentoDePago'));
		
		var montoComisionPagado = task.getVariable('corf_montoComisionPagado');
		var montoEmitidoFolio = execution.getVariable('corf:montoEmitidoFolio');
		if(montoEmitidoFolio == montoComisionPagado){
			changeValueCupoIfi(montoComisionPagado, true);
			//changeValueCupoCliente(montoComisionPagado, true);
			var textoRechazo = execution.getVariable('corf_rechazoReglas');
			if(!textoRechazo){
				execution.setVariable('corf_estadoTarea', 'Garantia_Constituida');
				task.setVariable('corf_estadoTarea', 'Garantia_Constituida');			
				
			}else{
				logger.error('completeRevisionFolio '+ textoRechazo);
				execution.setVariable('corf_estadoTarea', 'Garantia_no_Constituida_por_Vencimiento');
				task.setVariable('corf_estadoTarea', 'Garantia_no_Constituida_por_Vencimiento');
			}
			logger.error('fin completeRevisionFolio');
		}else{
			execution.setVariable('corf_rechazoReglas', 'El monto emitido y el monto pagado no son iguales');
			task.setVariable('corf_estadoTarea', 'Garantia_no_Constituida_por_Vencimiento');
			execution.setVariable('corf_estadoTarea', 'Garantia_no_Constituida_por_Vencimiento');
		}
		logger.error('estado: '+ execution.getVariable('corf_estadoTarea'));
	}
	
	function completeRevisionFolio(){
		logger.error('completeRevisionFolio');
		execution.setVariable('corf_estadoTarea',task.getVariable('corf_estadoTarea') );
//		if(task.getVariable('corfwf:revisionFolioDecision') == 'Garantia_Constituida'){
//			execution.setVariable('corf_estadoTarea', 'Garantia_Constituida');
//			task.setVariable('corf_estadoTarea', 'Garantia_Constituida');
//		}else{
//			execution.setVariable('corf_estadoTarea', 'Garantia_no_Constituida_por_Vencimiento');
//			task.setVariable('corf_estadoTarea', 'Garantia_no_Constituida_por_Vencimiento');
//		}
		logger.error('estado: '+ execution.getVariable('corf_estadoTarea'));
		
	}
	
	function takeFinRevisionFolio(){
		 logger.error('takeFinRevisionFolio ');
	}
	function finRevisionFolio(){
		 logger.error('finRevisionFolio ');
	}
	

	

	
	