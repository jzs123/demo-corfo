# Alfresco Platform JAR Module - SDK 3

To run use `mvn clean install -DskipTests=true alfresco:run` or `./run.sh` and verify that it 

 * Runs the embedded Tomcat + H2 DB 
 * Runs Alfresco Platform (Repository)
 * Runs Alfresco Solr4
 * Packages both as JAR and AMP assembly
 
 Try cloning it, change the port and play with `enableShare`, `enablePlatform` and `enableSolr`. 
 
 Protip: This module will work just fine as a Share module if the files are changed and 
 if the enablePlatform and enableSolr is disabled.
 
# Configuración de Hotswap

 1. Descargar Hotswap Installer [https://github.com/dcevm/dcevm/releases/download/light-jdk8u144%2B2/DCEVM-8u144-installer.jar](https://github.com/dcevm/dcevm/releases/download/light-jdk8u144%2B2/DCEVM-8u144-installer.jar)
 2. Instalarlo en la JDK haciendo `sudo java -jar DCEVM-light-8u112-installer.jar` o desde una cmd como adminsitrador, cygwin como adminsitrador sirve también
 3. Seleccionar la JDK con la que corre alfresco y accionar la opción `Install DCEVM as altjvm`
 4. Descargar [https://github.com/HotswapProjects/HotswapAgent/releases/download/1.1.0-SNAPSHOT/hotswap-agent-1.1.0-SNAPSHOT.jar](https://github.com/HotswapProjects/HotswapAgent/releases/download/1.1.0-SNAPSHOT/hotswap-agent-1.1.0-SNAPSHOT.jar)
 5. guardar el jar en un directorio ejemplo: `E:/Opt/hotswap/hotswap-agent-1.1.0-SNAPSHOT.jar`
 6. Exportar la variable ejemplo `linux,cygwin`
    1. editar .bashrc y agregar al final de la línea  `export MAVEN_OPTS="-javaagent:E:/Opt/hotswap/hotswap-agent-1.1.0-SNAPSHOT.jar"`

# Few things to notice

 * No parent pom
 * WAR assembly is handled by the Alfresco Maven Plugin configuration
 * Standard JAR packaging and layout
 * Works seamlessly with Eclipse and IntelliJ IDEA
 * JRebel for hot reloading, JRebel maven plugin for generating rebel.xml, agent usage: `MAVEN_OPTS=-Xms256m -Xmx1G -agentpath:/home/martin/apps/jrebel/lib/libjrebel64.so`
 * AMP as an assembly
 * [Configurable Run mojo](https://github.com/Alfresco/alfresco-sdk/blob/sdk-3.0/plugins/alfresco-maven-plugin/src/main/java/org/alfresco/maven/plugin/RunMojo.java) in the `alfresco-maven-plugin`
 * No unit testing/functional tests just yet
 * Resources loaded from META-INF
 * Web Fragment (this includes a sample servlet configured via web fragment)
 
# TODO
 
  * Abstract assembly into a dependency so we don't have to ship the assembly in the archetype
  * Purge, 
  * Functional/remote unit tests
   
  
 
