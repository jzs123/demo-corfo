1. Instalar Nginx
    
```
    wget http://nginx.org/download/nginx-1.13.8.zip
    cd c:\
    unzip nginx-1.13.8.zip
    cd nginx-1.13.8
    start nginx
```

2. Abrir el archivo nginx-1.13.8/conf/nginx.conf
    Cambiar la línea 44 a `root   e:/Workspace/corfo/sources;`

3. Instalar nvm
    https://github.com/coreybutler/nvm-windows/releases/download/1.1.6/nvm-setup.zip

4. Desde la cmd de windows ejecutar 
    ``nvm install 8``

5. npm install --global --production windows-build-tools
6. npm install
7. Eliminar el archivo package-lock.json
8. Agregar en el archivo package.json, session scripts el comando gulp
```
    "scripts": {
        "gulp": "gulp",
        "prestart": "bower install",
        "start": "npm install",
        "poststart": "gulp"
      },
```
9. npm run gulp serve

