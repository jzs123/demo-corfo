#!/bin/bash


APP=demo-garfo


d=`date +%Y%m%d`
c=`git rev-list --full-history --all --abbrev-commit | wc -l | sed -e 's/^ *//'`
h=`git rev-list --full-history --all --abbrev-commit | head -1`

REV=${c}.${h}.${d}
RELEASE=${APP}-${REV}
TAR_FILENAME=/tmp/${RELEASE}.tar.gz
QA_SERVER=54.227.182.96
QA_USER=ubuntu
IDENTITY=../alfrescoBCI.pem
PLATFORM_INSTALL_PATH=/usr/share/nginx/html/platform/webclient
TMP_DIR=`mktemp -d`
cd ~/workspace/corfo/webclient-spa
# npm run build
tar zcf ${TAR_FILENAME} app/ vendor/ server/ index.html

scp -i ${IDENTITY} ${TAR_FILENAME} $QA_USER@$QA_SERVER:/tmp
ssh -i ${IDENTITY} $QA_USER@$QA_SERVER /bin/bash << EOF
    mkdir $TMP_DIR
    cd $TMP_DIR
    echo "using: $TMP_DIR" 
    tar xf ${TAR_FILENAME} 
    [[ -d $PLATFORM_INSTALL_PATH/$APP/$RELEASE ]] && sudo rm -vr $PLATFORM_INSTALL_PATH/$APP/$RELEASE
    sudo mkdir -p $PLATFORM_INSTALL_PATH/$APP/$RELEASE 
    sudo mv -v app index.html vendor server $PLATFORM_INSTALL_PATH/$APP/$RELEASE 
    cd $PLATFORM_INSTALL_PATH/$APP 
    [[ -d $PLATFORM_INSTALL_PATH/$APP/current-release ]] && sudo rm current-release
    sudo ln -fs $RELEASE current-release 
    echo "sudo rm -rf $TMP_DIR"
EOF

