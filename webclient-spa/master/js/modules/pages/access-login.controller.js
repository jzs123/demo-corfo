/**=========================================================
 * Module: access-login.js
 * Demo for login api
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.pages')
        .controller('LoginFormController', LoginFormController);

    LoginFormController.$inject = [ 'alf', '$state', '$rootScope' ];
    function LoginFormController(alf, $state, $rootScope) {
        var vm = this;

        activate();

        ////////////////

        function activate() {
          // bind here all data from the form
          vm.account = {};
          // place the message if something goes wrong
          vm.authMsg = '';

          vm.login = function() {
            vm.authMsg = '';

            if(vm.loginForm.$valid) {

                alf.login(vm.account.email, vm.account.password)
                    .then(function(userObj) {
                        $rootScope.user = userObj;
                        $state.go('app.corfwf.tasks', { client: 0, type: 'corfwf:formPreAprobadoPorLote' });
                    })
                    .catch(function(err){
                        console.log(err);
                        vm.authMsg = 'Usuario o Clave incorrecta';
                        //vm.authMsg = 'Server Request Error';
                    });
            }
            else {
              // set as dirty if the user click directly to login so we show the validation messages
              /*jshint -W106*/
              vm.loginForm.account_email.$dirty = true;
              vm.loginForm.account_password.$dirty = true;
            }
          };
        }
    }
})();
