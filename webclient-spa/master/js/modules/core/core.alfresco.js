(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('alf', alfrescoClient);

    alfrescoClient.$inject = ['$http', '$cookies', '$localStorage', '$q', '$state' ];
    function alfrescoClient($http, $cookies, $localStorage, $q, $state){

        var 

        COOKIE_TICKET = 'alf_ticket',
        COOKIE_USER = 'alf_username',

        _strFormat = function (s, o, f, recurse) {
            var 
            i, j, /*k, */key, v, meta, saved=[], token, lidx=s.length,
            /*DUMP='dump', SPACE=' ',*/ LBRACE='{', RBRACE='}'//,
            /*dump, objstr*/;

            for (;;) {
                i = s.lastIndexOf(LBRACE, lidx);
                if (i < 0) {
                    break;
                }
                j = s.indexOf(RBRACE, i);
                if (i + 1 > j) {
                    break;
                }

                //Extract key and meta info
                token = s.substring(i + 1, j);
                key = token;
                
                // lookup the value
                v = o[key];

                // if a substitution function was provided, execute it
                if (f) {
                    v = f(key, v, meta);
                }

                if (!angular.isString(v) && !angular.isNumber(v)) {
                    // This {block} has no replace string. Save it for later.
                    v = "~-" + saved.length + "-~";
                    saved[saved.length] = token;

                    // break;
                }

                s = s.substring(0, i) + v + s.substring(j + 1);

                if (recurse === false) {
                    lidx = i-1;
                }

            }

            // restore saved {block}s
            for (i=saved.length-1; i>=0; i=i-1) {
                s = s.replace(new RegExp("~-" + i + "-~"), "{"  + saved[i] + "}", "g");
            }

            return s;
        },

        _alf = function(uri, params){
            return '/s' + _strFormat(uri, params || {} );
        },

        _getTicket = function(){

            return $cookies.get(COOKIE_TICKET);
        },

        _paramsTicket = function(){

            return { alf_ticket: _getTicket() };
        },

        _reqLogin = function(username, password){
            return {
                endpoint: _alf('/api/login'),
                body: { username: username, password: password }
            }
        },

        _reqUser = function(username, params){
            return {
                endpoint: _alf('/api/people/{username}', { username: username }),
                args: R.mergeAll([{ groups: true }, params, _paramsTicket() ])
            }
        },

        _reqUserAvatar = function(username){
            return {
                endpoint: _alf('/slingshot/profile/avatar/{username}', { username: username }),
                args: _paramsTicket()
            }
        },

        _reqUserSites = function(username, params){
            return {
                endpoint: _alf('/api/people/{username}/sites', { username: username }),
                args: R.mergeAll([{ roles: 'user', size: 100 }, params, _paramsTicket() ])
            }
        },

        _reqFormProcessor = function(type, id, body){
            return {
                endpoint: _alf('/api/{type}/{id}/formprocessor', { type: type, id: id }),
                args: _paramsTicket(),
                body: body
            }
        },

        _reqTaskInstances = function(params){
            return {
                endpoint: _alf('/api/task-instances'),
                args: R.mergeAll([
                    { authority: $cookies.get(COOKIE_USER), exclude: 'wcmwf:*,wf:*', skipCount: 0, maxItems: 100 }, 
                    params, 
                    _paramsTicket()
                ])
            };
        },

        _reqStartCorfWf = function(body){
            return {
                endpoint: _alf('/garfo/start-corfo'),
                args: _paramsTicket(),
                body: body
            }
        },

        _reqStartCorfRevFolio = function(siteId, montoFolio, body){
            return {
                endpoint: _alf('/corfo/formprocessorCreateRevisionFolio'),
                args: R.merge({ siteId: siteId, montoFolio: montoFolio }, _paramsTicket()),
                body: body
            };
        },

        _onResponseError = R.curry(function(req, err){

            if(err.status === 401){
                $state.go('page.login');
            }

            return $q.reject({ req: req, err: err });
        }),

        _get = function(req, config){
            return $http.get(req.endpoint, R.merge(config || {}, { params: req.args })).catch(_onResponseError(req));
        },

        _post = function(req, config){
            return $http.post(req.endpoint, req.body, R.merge(config || {}, { params: req.args })).catch(_onResponseError(req));
        },

        _login = function(user, pwd){

            return _post(_reqLogin(user, pwd))
                .then(function(resp){

                    $cookies.put(COOKIE_TICKET, resp.data.data.ticket);
                    $cookies.put(COOKIE_USER, user);
                    var req = _reqUserAvatar(user);
                    $localStorage.avatarUrl = req.endpoint + '?alf_ticket=' + req.args.alf_ticket;
                    return $q.all([ 
                        _get(_reqUser(user, { groups: true })),
                        _get(_reqUserSites(user)),
                    ]);
                })
                .then(function(resp){
                    $localStorage.user = resp[0].data;
                    $localStorage.userSites = resp[1].data;
                    return $localStorage.user;
                });
        },

        _logout = function(){
            // $og.cookie.remove('ticket');
            // $og.cookie.remove('user');
            // $og.session.$reset();
            // $og.state.go('login');
        },

        _formProcessor = function(type, id, params){ return _post(_reqFormProcessor(type, id, params)); },

        _taskInstances = function(params){ return _get(_reqTaskInstances(params)); },

        _startCorfWf = function(params){ return _post(_reqStartCorfWf(params)); },

        _startCorfRevFolioWf = function(siteId, montoFolio, formData){ 
            return _post( 
                _reqStartCorfRevFolio(siteId, montoFolio, formData), 
                { headers: { 'Content-Type': undefined }, transformRequest: R.identity }
            ); 
        };

        var ret = {

            strFormat: _strFormat,
            alf: _alf,
            login: _login,
            logout: _logout,
            formProcessor: _formProcessor,
            tasksInstances: _taskInstances,
            startCorfWf: _startCorfWf,
            startCorfRevFolioWf: _startCorfRevFolioWf
        };

        return ret;
    }

})();