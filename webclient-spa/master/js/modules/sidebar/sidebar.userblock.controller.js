(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .controller('UserBlockController', UserBlockController);

    UserBlockController.$inject = ['$scope', '$localStorage'];
    function UserBlockController($scope, $localStorage) {

        activate();

        ////////////////

        function activate() {

          $scope.userBlockVisible = true;
          $scope.avatarUrl = $localStorage.avatarUrl;
          $scope.userInfo = $localStorage.user;
          var detach = $scope.$on('toggleUserBlock', function(/*event, args*/) {

            $scope.userBlockVisible = ! $scope.userBlockVisible;

          });

          $scope.$on('$destroy', detach);
        }
    }
})();
