(function() {
    'use strict';

    angular
        .module('garfo')
        .directive('rutValidator', rutValidator);

    function rutValidator() {
        return {
          require: 'ngModel',
          link: function(scope, elem, attr, ngModel) {
              //For DOM -> model validation
              ngModel.$parsers.unshift(function(value) {

                if($.validateRut(value)) {
                    console.log("El rut es válido!");
                } else {
                    console.log('rut invalido');
                }
                 var valid = $.validateRut(value);
                 ngModel.$setValidity('rutValidator', valid);
                 return valid ? value : undefined;
              });

              //For model -> DOM validation
              ngModel.$formatters.unshift(function(value) {
                 ngModel.$setValidity('rutValidator', $.validateRut(value));
                 return value;
              });
          }
       };
    }
})();