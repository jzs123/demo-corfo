(function() {
    'use strict';

    angular
        .module('garfo')
        .directive('maxAllowed', maxAllowed);

    maxAllowed.$inject = ['$parse'];
    function maxAllowed($parse) {
        return {
          require: 'ngModel',
          link: function(scope, elem, attr, ngModel) {

                var maxValue = $parse(attr.maxAllowed)(scope);

                //For DOM -> model validation
                ngModel.$parsers.unshift(function(value) {
                    var valid = value <= maxValue;
                    ngModel.$setValidity('maxAllowed', value <= maxValue);
                    return value;
                });

                //For model -> DOM validation
                ngModel.$formatters.unshift(function(value) {
                    ngModel.$setValidity('maxAllowed', value <= maxValue);
                    return value;
                });
          }
       };
    }
})();