(function() {
    'use strict';

    angular
        .module('garfo')
        .directive('fileChange', fileChange);

    function fileChange() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var onChangeHandler = scope.$eval(attrs.fileChange);
                element.on('change', onChangeHandler);
                element.on('$destroy', function() {
                    element.off();
                });
            }
        };
    }
})();