(function() {
    'use strict';

    angular
        .module('garfo')
        .directive('dynController', DynController);

    DynController.$inject = ['$compile', '$parse'];
    function DynController($compile, $parse) {
        return {
            restrict: 'A',
            terminal: true,
            priority: 100000,
            link: function(scope, elem, attrs) {
                    // Parse the scope variable
                    var name = $parse(elem.attr('dyn-controller'))(scope);
                    elem.removeAttr('dyn-controller');
                    elem.attr('ng-controller', name);

                    // Compile the element with the ng-controller attribute
                    $compile(elem)(scope);       
            }
        }
    }
})();