(function() {
    'use strict';

    angular
        .module('garfo')
        .provider('Form', formProvider);

    formProvider.$inject = ['RouteHelpersProvider'];
    function formProvider(helper){

        var 
        
        _formDefinitions = {},

        _getFormDef = function(id){
            if(!_formDefinitions[id]){
                throw new Error('El formulario ' + id + ' no esta definido');
            }
            return _formDefinitions[id];
        },

        _form = function(formDef){
            
            if(_formDefinitions[formDef.id]){
                throw new Error('Ya existe un formulario definido: ' + formDef.id);
            }

            _formDefinitions[formDef.id] = R.merge(formDef, {
                templateUrl: helper.basepath('form/' + formDef.kind + '/' + formDef.mode + '-' + formDef.name + '.html'),
            });
            return this;
        };

        this.form = _form;

        this.$get = function(){

            return {
                getFormDef: _getFormDef
            }
        };
    }

})();