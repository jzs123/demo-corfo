(function() {
    'use strict';

    angular
        .module('garfo')
        .config(formConfig);

    formConfig.$inject = ['FormProvider', 'RouteHelpersProvider'];
    function formConfig(formProvider, helper){

        formProvider
            .form({
                id: 'activiti$corfoWorkflow',
                name: 'activiti-worflow',
                kind: 'workflow',
                mode: 'create-multiple',
                controller: 'FormWorkflowActivitiCorfo as formCorfwf',
            })
    }

})();