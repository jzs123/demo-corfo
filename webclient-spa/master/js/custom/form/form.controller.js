(function() {
    'use strict';

    angular
        .module('garfo')
        .controller('FormController', Controller);

    Controller.$inject = ['$scope', '$state', 'Form' ];
    function Controller($scope, $state, Form) {
        // for controllerAs syntax
        var vm = this;

        activate();

        ////////////////

        function activate() {
            
            vm.definition = Form.getFormDef($state.params.formId);
        }
    }
})();