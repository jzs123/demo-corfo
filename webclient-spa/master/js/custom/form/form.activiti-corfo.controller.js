(function() {
    'use strict';

    angular
        .module('garfo')
        .controller('FormWorkflowActivitiCorfo', Controller);

    Controller.$inject = [
        '$scope', '$log', '$window','$timeout', 'FileUploader', 'COLUMNS_FINANCIAL_COVERS', 'CorfWfUtils', 'alf', 'Notify', '$state', 
        '$localStorage'
    ];
    function Controller(
        $scope, $log, $window, $timeout, FileUploader, COLUMNS_FINANCIAL_COVERS, utils, alf, Notify, $state, $localStorage
    ) {
       
        // for controllerAs syntax
        var vm = this;

        vm.model = {
            dListRutIfi: '24.952.044-6'
        };

        activate();

        ////////////////

        function activate() {
            var resizeEvent = 'resize.ag-grid';
            var $win = $($window); // cache reference for resize
            var _list = [];
            // var uploader = vm.uploader = new FileUploader();

            vm.onUploadFile = function(e) {
                var file = e.target.files[0];
                if(!file){
                    vm.model.fileInput = undefined;
                    return;
                }
                var rABS = true;
                var reader = new FileReader();
                reader.onload = function(e) {
                    
                    var data = e.target.result;
                    if(!rABS) 
                        data = new Uint8Array(data);

                    var wb = XLSX.read(data, { type: rABS ?'binary' :'array' });

                    var wsname = wb.SheetNames[0];
                    var ws = wb.Sheets[wsname];

                    var numLote = alf.strFormat('LOTE-{lote}', {
                        lote: moment().format('YYYYMMDD-HHmm')  
                    });
                    
                    var parse = R.compose( 
                        R.map(R.compose( utils.rowToObj, R.concat([ numLote ] ))), 
                        R.filter(R.complement(R.isEmpty)), 
                        R.slice(1, Infinity)
                    );

                    _list = parse(XLSX.utils.sheet_to_json(ws, { header:1 }));

                    vm.gridOptions.api.setRowData(_list);
                    vm.showSendButton = _list.length > 0;
                };

                if(rABS) {
                    reader.readAsBinaryString(file); 
                }
                else {
                    reader.readAsArrayBuffer(file);
                }

                $scope.$apply(function(){
                    vm.model.fileInput = file;  
                })

                $log.log('I\'m a line from FormWorkflowActivitiCorfo.js');
            };

            var cols = R.clone(COLUMNS_FINANCIAL_COVERS);
            cols[0] = R.merge(cols[0], { checkboxSelection: false, headerCheckboxSelection: false });

            vm.gridOptions = {
                columnDefs: cols,
                rowData: null,
                suppressRowClickSelection: true,
                
                // enableFilter: true,
                onGridReady: function(params) {
                    params.api.sizeColumnsToFit();
                    $win.on(resizeEvent, function() {
                        $timeout(function(){
                            params.api.sizeColumnsToFit();
                        });
                    });
                }

            };

            // vm.submitted = false;
            vm.validateInput = function(name, type) {
                var input = vm.uploadMultiple[name];
                return input.$error[type];
            };

            vm.onSubmitUploadMultiple = function(){
                vm.submitting = true;

                function f(pos){
                    if(pos === _list.length){
                        Notify.alert('Procesos iniciados correctamente', { status: 'success' });
                        $state.go('app.corfwf.tasks', { type: 'corfwf:formPreAprobadoPorLote' });
                        return;
                    }
                    var site = $localStorage.userSites[$state.params.client];
                    var props = R.merge(_list[pos], { 
                        prop_corf_dListRutIfi: vm.model.dListRutIfi, 
                        prop_corf_siteIfi: site.shortName
                    });

                    alf
                        .startCorfWf(props)
                        .then(function(resp){
                            console.log('Success el envio de la fila ' + pos, _list[pos], resp);
                            f(pos + 1);
                        })
                        .catch(function(err){
                            console.error(err);
                            //f(pos + 1)
                        });
                }

                f(0, []);
            };

            $scope.$watch('formCorfwf.uploadMultiple.dListRutIfi.$valid', function(newVal, oldVal){
                if(newVal === true){
                    $timeout(function() {
                        if(vm.model.dListRutIfi){
                            vm.model.dListRutIfi = $.formatRut(vm.model.dListRutIfi);
                        }
                    });
                }
            })
        }
    }
})();