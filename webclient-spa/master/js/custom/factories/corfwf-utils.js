(function() {
    'use strict';

    angular
        .module('garfo')
        .factory('CorfWfUtils', CorfWfUtils);

    CorfWfUtils.$inject = ['$log'];
    function CorfWfUtils($log) {
        
        var 

        renameBy = R.curry(function(fn, obj){
            return R.pipe(R.toPairs, R.map(R.adjust(fn, 0)), R.fromPairs)(obj);
        }),

        renameByProp = renameBy(R.concat('prop_')),

        _rowProps = [
            'prop_corf_numeroLote',
            'prop_corf_numeroOperacionIfi', 
            'prop_corf_rut', 
            'prop_corf_dListSectorEconomico', 
            'prop_corf_ventasEmpresaAnual', 
            'prop_corf_localizacion', 
            'prop_corf_clasifRiesgoEmpresa', 
            'prop_corf_dListTipoOperacion', 
            'prop_corf_dListMoneda', 
            'prop_corf_montoDeOperacion', 
            'prop_corf_montoTotalCapital', 
            'prop_corf_montoOperacionSinComision', 
            'prop_corf_planPago', 
            'prop_corf_fechaPrimerPago', 
            'prop_corf_mesesPeriodoGracia', 
            'prop_corf_frecuenciaPago', 
            'prop_corf_numeroCuotas', 
            'prop_corf_tasaInteresAnual',
            'prop_corf_fechaAprobacion', 
            'prop_corf_garantiasAdicionales', 
            'prop_corf_descripcionSeguro', 
            'prop_corf_genero', 
            'prop_corf_contacto', 
            'prop_corf_tipoGracia'
        ],

        _parseDate = function(v){ return moment(v, 'YYYYMMDD').format(); },

        _parseProps = { prop_corf_fechaPrimerPago: _parseDate, prop_corf_fechaAprobacion: _parseDate },

        _rowToObj = R.compose(R.evolve(_parseProps), R.zipObj(_rowProps)),
        
        _addTaskId = function(task) { return R.assoc( 'taskId', task.id ) },

        _addTaskName = function(task){ return R.assoc('taskName', task.name) },

        _addInstanceId = function(task){ return R.assoc('instanceId', task.workflowInstance.id )},

        _taskToObj = function(task){ 
            return R.compose( 
                _addInstanceId(task),
                _addTaskId(task),
                _addTaskName(task), 
                renameByProp, 
                R.prop('properties')
             )(task);
         },

        _taskOrigProps = R.pickBy(function(v, k){ return k !== 'taskName' && k !== 'taskId'; });

        return {
            rowToObj: _rowToObj,
            taskToObj: _taskToObj,
            taskOrigProps: _taskOrigProps
        };
    }
})();