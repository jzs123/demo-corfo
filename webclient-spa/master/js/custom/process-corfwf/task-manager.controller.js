(function() {
    'use strict';

    angular
        .module('garfo')
        .controller('CorfWfController', Controller);

    Controller.$inject = [ 
        '$scope', '$http', '$window', '$timeout', '$state', 'alf', 'CorfWfUtils', 'COLUMNS_FINANCIAL_COVERS', 'COLUMNS_REV_FOLIO', 'Notify',
        '$rootScope', 'ngDialog'
    ];
    function Controller(
        $scope, $http, $window, $timeout, $state, alf, utils, COLUMNS_FINANCIAL_COVERS, COLUMNS_REV_FOLIO, Notify, $rootScope, ngDialog
    ) {
        
        var 
        
        // for controllerAs syntax
        vm = this,

        $win,

        //constants
        RESIZE_EVENT = 'resize.ag-grid',


        _defGridOptions = function(columnDefs){
            return {
                columnDefs: columnDefs || R.clone(COLUMNS_FINANCIAL_COVERS),
                rowData: null,
                rowSelection: 'multiple',
                // enableFilter: true,
                onGridReady: function(params) {
                    params.api.sizeColumnsToFit();
                    $win.on(RESIZE_EVENT, function() {
                        $timeout(function(){
                            params.api.sizeColumnsToFit();
                        });
                    });
                },

                onRowSelected: function(params){

                    console.log(params);

                    vm.gridOptions.api.sizeColumnsToFit();
                    $timeout(function(){
                        // vm.gridOptions.api.
                        // var params = {
                        //     force: isForceRefreshSelected(),
                        //     rowNodes: rowNodes
                        // };
                        // vm.gridOptions.api.refreshCells({ force: true, rowNodes: [ params.node ] });
                        params.node.setDataValue('selected', true);
                        if(vm.selectedNode){
                            vm.selectedNode.setDataValue('selected', false);
                        }
                        vm.selectedNode = params.node;
                    });
                },

            }
        },

        _getRowClassRulesPreAprobado = {
            'bg-warning-dark': function(params){ return !params.node.selected && params.data.prop_corf_rechazoReglas }
        },

        _reducerAmount = function(acc, node){ return acc + node.data.prop_corf_montoDeOperacion; },

        _reduceAmount = R.reduce(_reducerAmount, 0),

        _onRowSelectedPreApproveCorfo = function(params){

            // console.log(params);

            vm.gridOptions.api.sizeColumnsToFit();
            $timeout(function(){
                // vm.gridOptions.api.
                // var params = {
                //     force: isForceRefreshSelected(),
                //     rowNodes: rowNodes
                // };
                // vm.gridOptions.api.refreshCells({ force: true, rowNodes: [ params.node ] });
    
                $timeout(function(){ vm.selectedAmount = _reduceAmount(params.api.getSelectedNodes()); });

                params.node.setDataValue('selected', true);
                if(vm.selectedNode){
                    vm.selectedNode.setDataValue('selected', false);
                }

                vm.selectedNode = params.node;
            });
        },

        _fetchTasks = function(){
            var types = [ 
                'corfwf:formPreAprobadoPorLote', 
                'corfwf:formEmisionFolio', 
                'corfwf:formPreAprobacionCorfo',
                'corfwf:formRevisionFolio'
            ];
            var excludeTypes = R.filter(function(type){ return type !== $state.params.type; }, types).join();
            var exclude = alf.strFormat('wcmwf:*,wf:*,{excludeTypes}', { excludeTypes: excludeTypes   });

            return alf.tasksInstances({ exclude: exclude })
                .then(function(resp){
                    vm.gridOptions.api.setRowData(R.map(utils.taskToObj, resp.data.data));
                })
                .catch(function(err){
                    console.error(err);
                });
        },

        _wfOutput = function(outputProp, outputVal, flag){
            // return alf.formProcessor('task', )
            var selections = vm.gridOptions.api.getSelectedNodes();
            vm[flag] = true;
            function f(pos){
                if(pos === selections.length){
                    vm[flag] = false;
                    Notify.alert(  'Tareas iniciados avanzados satisfactoriamente', { status: 'success' });
                    //$state.go('app.corfwf.tasks', { type: 'corfwf:formPreAprobado' }, { reload: true });
                    _fetchTasks();
                    return;
                }
                var props = selections[pos].data;

                alf
                    .formProcessor('task', props.taskId, R.assoc(outputProp, outputVal, {  prop_transitions: 'Next' }))
                    .then(function(resp){
                        // console.log('Success el envio de la fila ' + pos, _list[pos], resp);

                        f(pos + 1);
                    })
                    .catch(function(err){
                        console.error(err);
                        //f(pos + 1)
                        vm[flag] = false;
                    })
            }

            f(0);
        },

        _openModalFolio = function(){
            $rootScope.theme = 'ngdialog-theme-default';
            console.log('here!');
            ngDialog.open({
                  template: 'withInlineController',
                  controller: ['$scope', '$timeout', '$localStorage', function ($scope, $timeout, $localStorage) {
                        // vm.submitted = false;
                        $scope.validateInput = function(name, type) {
                            var input = $scope.formAmount[name];
                            return input.$error[type];
                        };

                        $scope.onUploadFileAmount = function(e){
                            $scope.$apply(function(){
                                var file = e.target.files[0];
                                $scope.model.fileInputAmount = file;  
                                console.log(file);
                            })
                        };

                        $scope.model = { selectedAmount: vm.selectedAmount, amountValue: vm.selectedAmount };

                        $scope.emitFolio = function(){
                            var client = $state.params.client;
                            var site = $localStorage.userSites[client];
                           
                            var nodes = vm.gridOptions.api.getSelectedNodes();
                            var ids = R.map(function(node){ return node.data.instanceId.replace('activiti$', ''); }, nodes).join();
                            var formData = new FormData();
                            formData.append('worflowsIds', ids);
                            formData.append('filedata', $scope.model.fileInputAmount);

                            alf.startCorfRevFolioWf(site.shortName, $scope.model.amountValue, formData).then(function(resp){
                                console.log(formData);
                                $scope.closeThisDialog();    
                            });
                            
                        };

                  }],
                  className: 'ngdialog-theme-default'
            });
        };

        activate();

        ////////////////

        function _configureView(){

            var _gridOpts;

            switch($state.params.type){
                case 'corfwf:formPreAprobadoPorLote':
                    vm.showBtnApproveLote = true;
                    vm.showBtnRejectLote = true;
                    vm.header = 'Aprobar/Rechazar Lote';
                    vm.headerDesc = 'Lista de tareas pendientes de aprobar/rechazar lote';
                    _gridOpts =  _defGridOptions();
                    // _gridOpts.getRowClass = _getRowClassPreAprobado;
                    _gridOpts.rowClassRules = _getRowClassRulesPreAprobado;
                    vm.gridOptions = _gridOpts;
                    break;

                case 'corfwf:formEmisionFolio':
                     vm.selectedAmount = 0;
                    vm.showBtnEmitFolio = true;
                    vm.header = 'Emitir Folio';
                    vm.headerDesc = 'Lista de tareas pendientes de emitir folio';
                    _gridOpts =  _defGridOptions();
                    _gridOpts.onRowSelected = _onRowSelectedPreApproveCorfo;
                    vm.gridOptions = _gridOpts;
                    break;

                case 'corfwf:formPreAprobacionCorfo':
                    vm.selectedAmount = 0;
                    vm.showBtnAmount = true;
                    vm.header = 'Pre Aprobar Corfo';
                    vm.headerDesc = 'Lista de tareas pendientes de pre aprobar corfo';
                    _gridOpts =  _defGridOptions();
                    _gridOpts.onRowSelected = _onRowSelectedPreApproveCorfo;
                    vm.gridOptions = _gridOpts;
                    break

                case 'corfwf:formRevisionFolio':
                    vm.header = 'Revisar Folio';
                    vm.headerDesc = 'Lista de tareas pendientes de revisión de folio';
                    _gridOpts =  _defGridOptions(COLUMNS_REV_FOLIO);
                    vm.gridOptions = _gridOpts;
                    break;
            }
        }


        function activate() {
            console.log('activate -- CorfWfController')
            $win = $($window); // cache reference for resize
            _configureView();

            vm.formParams = { kind: 'workflow', formId: 'activiti$corfoWorkflow', mode: 'create-multiple' };

            _fetchTasks();

            vm.wfOutput = _wfOutput;

            vm.openModalFolio = _openModalFolio;
            //-----------------------------
            // Get the data from SERVER
            //-----------------------------

            // turn off event
            $scope.$on('$destroy', function(){
                $win.off(RESIZE_EVENT);
            })
            
        }
    }
})();