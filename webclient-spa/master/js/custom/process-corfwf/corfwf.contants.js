(function() {
    'use strict';

    angular
        .module('garfo')
        .constant('COLUMNS_FINANCIAL_COVERS', [
            { 
                headerName: 'NumLote', 
                field: 'prop_corf_numeroLote', 
                width: 200, 
                minWidth: 200, 
                filter: 'set', 
                checkboxSelection: true,
                headerCheckboxSelection: function(params) {
                    var displayedColumns = params.columnApi.getAllDisplayedColumns();
                    var thisIsFirstColumn = displayedColumns[0] === params.column;
                    return thisIsFirstColumn;
                }
            },
            { 
                headerName: 'Num_OP_IFI', 
                field: 'prop_corf_numeroOperacionIfi', 
                width: 200, 
                minWidth: 200 
            },
            { 
                headerName: 'Rut', 
                field: 'prop_corf_rut', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'Sector Comercial', 
                field: 'prop_corf_dListSectorEconomico', 
                width: 200, 
                minWidth: 200 
            }, 
            { 
                headerName: 'Ventas empresa', 
                field: 'prop_corf_ventasEmpresaAnual', 
                width: 200, 
                minWidth: 200
            }, 
            { 
                headerName: 'Localizacion', 
                field: 'prop_corf_localizacion', 
                width: 200, 
                minWidth: 200 
            }, 
            { 
                headerName: 'Clasificacion Riesgo', 
                field: 'prop_corf_clasifRiesgoEmpresa', 
                width: 200, 
                minWidth: 200 
            }, 
            { 
                headerName: 'Tipo Operación', 
                field: 'prop_corf_dListTipoOperacion', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'moneda_operacion', 
                field: 'prop_corf_dListMoneda', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'monto_operacion_moneda_origen', 
                field: 'prop_corf_montoDeOperacion', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'monto_capital_trabajo_y_o_inversion', 
                field: 'prop_corf_montoTotalCapital', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'monto_operacion_sin_comision', 
                field: 'prop_corf_montoOperacionSinComision', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'tipo_plan_de_pago', 
                field: 'prop_corf_planPago', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'fecha_primer_vencimiento', 
                field: 'prop_corf_fechaPrimerPago', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'meses_periodo_gracia', 
                field: 'prop_corf_mesesPeriodoGracia', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'frecuencia_pagos', 
                field: 'prop_corf_frecuenciaPago', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'numero_cuotas', 
                field: 'prop_corf_numeroCuotas', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'tasa_interes_anual_operacion', 
                field: 'prop_corf_tasaInteresAnual', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'fecha_aprobacion_operacion', 
                field: 'prop_corf_fechaAprobacion', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'garantias_adicionales', 
                field: 'prop_corf_garantiasAdicionales', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'descripcion_seguro', 
                field: 'prop_corf_descripcionSeguro', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'Genero', 
                field: 'prop_corf_genero', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'datos_contacto', 
                field: 'prop_corf_contacto', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'Tipo_gracia', 
                field: 'prop_corf_tipoGracia', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            } 
        ])
        .constant('COLUMNS_REV_FOLIO', [
            { 
                headerName: 'Folio', 
                field: 'prop_corf_folio', 
                width: 200, 
                minWidth: 200 
            },
            { 
                headerName: 'SAP', 
                field: 'prop_corf_sap', 
                width: 200, 
                minWidth: 200, 
                filter: 'number' 
            }, 
            { 
                headerName: 'Comisión Pagada', 
                field: 'prop_corf_montoComisionPagado', 
                width: 200, 
                minWidth: 200 
            }, 
            { 
                headerName: 'comprobante de Pago', 
                field: 'prop_corf_documentoDePago', 
                width: 200, 
                minWidth: 200
            }
        ]);
})();